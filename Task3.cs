using System;

public class RangeArray
{
	private int[] _array;
	private int _lowerIndex;
	private int _upperIndex;
	
	public RangeArray(int size)
	{
		this._lowerIndex = 0;
		this._upperIndex = size - 1;
		this.Length = size;
		this._array = new int[size];
	}
	
	public RangeArray(int lowerIndex, int upperIndex)
	{
		if(lowerIndex >= upperIndex)
		{
			// invalid indexes
			lowerIndex = 0;
			upperIndex = 1;
			this.Length = 1;			
		}
		else
		{
			this._lowerIndex = lowerIndex;
			this._upperIndex = upperIndex;			
			this.Length = upperIndex - lowerIndex + 1;
		}
		this._array = new int[this.Length];
	}
	
	public int Length { get; private set;}
	
	public int this[int index]
	{		
		get
		{
			if(!this.IndexIsInBounds(index))
			{
				throw new IndexOutOfRangeException("Invalid index!");
			}
			else
			{				
				return this._array[index - this._lowerIndex];
			}
		}
		set
		{
			if(!this.IndexIsInBounds(index))
			{
				throw new IndexOutOfRangeException("Invalid index!");
			}
			else
			{				
				this._array[index - this._lowerIndex] = value;
			}
		}
	}
	
	public static RangeArray operator+ (RangeArray array1, RangeArray array2)
	{
		if(array1.Length != array2.Length)
		{
			throw new FormatException("Two arrays have different index ranges!");
		}
		
		if(!IndexesAreEqual(array1, array2))
		{
			throw new FormatException("Two arrays have different index ranges!");
		}
		
		RangeArray result = new RangeArray(array1._lowerIndex, array1._upperIndex);
		
		for (var i = array1._lowerIndex; i <= array1._upperIndex; i++)
		{
			result[i] = array1[i] + array2[i];
		}
		
		return result;		
	}
	
	public static RangeArray operator- (RangeArray array1, RangeArray array2)
	{
		if(array1.Length != array2.Length)
		{
			throw new FormatException("Two arrays have different index ranges!");
		}
		
		if(!IndexesAreEqual(array1, array2))
		{
			throw new FormatException("Two arrays have different index ranges!");
		}
		
		RangeArray result = new RangeArray(array1._lowerIndex, array1._upperIndex);
		
		for (var i = array1._lowerIndex; i <= array1._upperIndex; i++)
		{
			result[i] = array1[i] - array2[i];
		}
		
		return result;		
	}
	
	public static RangeArray operator* (RangeArray array, int scalar)
	{
		for (var i = array._lowerIndex; i <= array._upperIndex; i++)
		{
			array[i] *= scalar;
		}
		return array;
	}
	
	public static RangeArray Addition(RangeArray array1, RangeArray array2)
	{
		if(array1.Length != array2.Length)
		{
			throw new FormatException("Two arrays have different index ranges!");
		}
		
		if(!IndexesAreEqual(array1, array2))
		{
			throw new FormatException("Two arrays have different index ranges!");
		}
		
		RangeArray result = new RangeArray(array1._lowerIndex, array1._upperIndex);
		
		for (var i = array1._lowerIndex; i <= array1._upperIndex; i++)
		{
			result[i] = array1[i] + array2[i];
		}
		
		return result;
	}
	
	public static RangeArray Subtraction(RangeArray array1, RangeArray array2)
	{
		if(array1.Length != array2.Length)
		{
			throw new FormatException("Two arrays have different index ranges!");
		}
		
		if(!IndexesAreEqual(array1, array2))
		{
			throw new FormatException("Two arrays have different index ranges!");
		}
		
		RangeArray result = new RangeArray(array1._lowerIndex, array1._upperIndex);
		
		for (var i = array1._lowerIndex; i <= array1._upperIndex; i++)
		{
			result[i] = array1[i] - array2[i];
		}
		
		return result;		
	}	
	
	public static RangeArray MultiplicationByScalar(RangeArray array, int scalar)
	{
		for (var i = array._lowerIndex; i <= array._upperIndex; i++)
		{
			array[i] *= scalar;
		}
		return array;
	}
	
	public void Add(RangeArray arrayToAdd)
	{
		if(this.Length != arrayToAdd.Length)
		{
			Console.WriteLine("Two arrays can not be added!");
			return;
		}
		
		if(!IndexesAreEqual(this, arrayToAdd))
		{
			Console.WriteLine("Two arrays can not be added!");
			return;
		}
							
		for (var i = 0; i < this.Length; i++)
		{
			this._array[i] += arrayToAdd._array[i];
		}				
	}
	
	public void Subtract(RangeArray arrayToSubtract)
	{
		if(this.Length != arrayToSubtract.Length)
		{
			Console.WriteLine("Two arrays can not be subtracted!");
			return;
		}
		
		if(!IndexesAreEqual(this, arrayToSubtract))
		{
			Console.WriteLine("Two arrays can not be subtracted!");
			return;
		}
							
		for (var i = 0; i < this.Length; i++)
		{
			this._array[i] -= arrayToSubtract._array[i];
		}
	}
	
	public void MultiplyByScalar(int scalar)
	{
		for (var i = 0; i < this.Length; i++)
		{
			this._array[i] *= scalar;
		}	
	}
	
	public override bool Equals(object obj)
	{
		if (obj == null)
		{
			return false;
		}
		
		RangeArray arrayToCompare = obj as RangeArray;
		if(arrayToCompare == null)
		{
			return false;
		}
		
		if(this.Length != arrayToCompare.Length)
		{
			return false;
		}
		
		if(!IndexesAreEqual(this, arrayToCompare))
		{
			return false;
		}
		
		for (var i = this._lowerIndex; i <= this._upperIndex; i++)
        {
            if (this[i] != arrayToCompare[i])
            {				
				return false;
			}
		}
		
		return true;
	}
	
	public override int GetHashCode()
	{
		return this._array.GetHashCode();
	}
	
	public void RandomInitializeArray(int minValue = 1, int maxValue = 10)
	{
		Random random = new Random();
		for (var i = 0; i < this.Length; i++)
		{
			this._array[i] = random.Next(minValue, maxValue);
		}
	}
	
	public void ConsecutiveInitializeArray(int minElement = 0)
	{
		for (var i = 0; i < this.Length; i++)
		{
			this._array[i] = ++minElement;
		}
	}	
	
	public override string ToString()
	{
		return String.Join(" ", this._array);
	}
	
	private static bool IndexesAreEqual(RangeArray array1, RangeArray array2)
	{
		if(array1._lowerIndex == array2._lowerIndex && array1._upperIndex == array2._upperIndex)
		{
			return true;
		}
		return false;
	}
	
	private bool IndexIsInBounds(int index)
	{
		if(index >= this._lowerIndex & index <= this._upperIndex)
		{
			return true;
		}		
		return false;		
	}	
}

class Program
{
	static void Main()
	{	
		Console.WriteLine("Hello! This is my program #3!\n");
		RangeArray array1 = new RangeArray(-1, 9);
		RangeArray array2 = new RangeArray(-1, 9);
		RangeArray array3 = new RangeArray(11);
		RangeArray array4 = new RangeArray(-5, 5);
		
		Console.WriteLine("I've just created 4 arrays with the same length.");
		Console.WriteLine("Let's initialize them.");
		
		array1.RandomInitializeArray(2, 5);
		array2.ConsecutiveInitializeArray();
		array3.ConsecutiveInitializeArray(3);
		array4.RandomInitializeArray();
		
		Console.WriteLine("\narray1: \n{0}", array1.ToString());		
		Console.WriteLine("\narray2: \n{0}", array2.ToString());		
		Console.WriteLine("\narray3: \n{0}", array3.ToString());
		Console.WriteLine("\narray4: \n{0}", array4.ToString());
		
		Console.WriteLine("\nWe can try to change arrays like this - array2[index] = value");
		try
		{
			array2[-1] = 10;
			array2[3] = 20;
			Console.WriteLine("array2 after changing: \n{0}", array2.ToString());			
		}
		catch(IndexOutOfRangeException exception)
		{
			Console.WriteLine(exception.Message);
		}
		
		Console.WriteLine("\nDo it one more time!");
		try
		{
			array2[-10] = 10;			
			Console.WriteLine("\narray2 after changing: \n{0}", array2.ToString());
		}
		catch(IndexOutOfRangeException exception)
		{
			Console.WriteLine(exception.Message);
			Console.WriteLine("Ups! Wrong index :)");
		}
		
		
		Console.WriteLine("\nLet's add and subtract two arrays!");
		
		RangeArray result;
		try
		{
			Console.WriteLine("\narray1 + array2:");
			result = array1 + array2;
			Console.WriteLine(result.ToString());
			
			Console.WriteLine("\narray1 - array2:");
			result = array1 - array2;
			Console.WriteLine(result.ToString());
		}
		catch(FormatException exception)
		{
			Console.WriteLine(exception.Message);
		}
		
		Console.WriteLine("\nLet's add and subtract another arrays!");
		
		try
		{
			Console.WriteLine("\narray2 + array4:");		
			result = array2 + array4;
			Console.WriteLine(result.ToString());
			
			Console.WriteLine("\narray2 - array4:");		
			result = array2 - array4;
			Console.WriteLine(result.ToString());
		}
		catch(FormatException exception)
		{
			Console.WriteLine(exception.Message);
			Console.WriteLine("Ups! Different boundaries:)\n");
		}
		
		Console.WriteLine("Let's multiply array3 by 3:");
		array3 *= 3;
		Console.WriteLine(array3.ToString());
		
		Console.WriteLine("\nLet's try that actions with static methods!");
		try
		{
			Console.WriteLine("\narray1 + array2:");
			result = RangeArray.Addition(array1, array2);
			Console.WriteLine(result.ToString());
			
			Console.WriteLine("\narray1 - array2:");
			result = RangeArray.Subtraction(array1, array2);
			Console.WriteLine(result.ToString());
		}
		catch(FormatException exception)
		{
			Console.WriteLine(exception.Message);
		}
		
		Console.WriteLine("\nOne more time!");
		try
		{
			Console.WriteLine("\narray3 + array4:");
			result = RangeArray.Addition(array3, array4);
			Console.WriteLine(result.ToString());
			
			Console.WriteLine("\narray3 - array4:");
			result = RangeArray.Subtraction(array3, array4);
			Console.WriteLine(result.ToString());
		}
		catch(FormatException exception)
		{
			Console.WriteLine(exception.Message);
			Console.WriteLine("Ups! Different boundaries:)\n");
		}
		
		Console.WriteLine("Let's again multiply array3 by 3 but with static method:");
		RangeArray.MultiplicationByScalar(array3, 3);
		Console.WriteLine(array3.ToString());
		
		Console.WriteLine("\nLet's change arrays with non-static methods!");
		Console.WriteLine("\narray1 = array1 + array2 :");
		array1.Add(array2);
		Console.WriteLine(array1.ToString());
		
		Console.WriteLine("\narray2 = array2 - array1 :");
		array2.Subtract(array1);
		Console.WriteLine(array2.ToString());
		
		Console.WriteLine("\narray1 = array1 + array3 :");
		array1.Add(array3);
		
		Console.WriteLine("\narray4 * 2:");
		array4.MultiplyByScalar(2);
		Console.WriteLine(array4.ToString());
		
		Console.WriteLine("\nLet's check method Equals()!");
		array1.ConsecutiveInitializeArray();
		array2.ConsecutiveInitializeArray();
		
		Console.WriteLine("\narray1:");
		Console.WriteLine(array1.ToString());
		Console.WriteLine("\narray2:");
		Console.WriteLine(array2.ToString());
		Console.WriteLine("\narray3:");
		Console.WriteLine(array3.ToString());
		
		Console.WriteLine("\narray1 == array2 : {0}", array1.Equals(array2));
		Console.WriteLine("array1 == array3 : {0}", array1.Equals(array3));		
	}
}